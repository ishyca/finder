/**
* main.js
*/



$(function() {
    console.log( "ready!" );


    /* -- Leaft map --*/
    var map = L.map('map').setView([45.498, -73.556,], 16);

    L.tileLayer('http://{s}.tiles.mapbox.com/v4/{mapId}/{z}/{x}/{y}.png?access_token={token}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    mapId:'ishyca.ld3f389e',
    token:'pk.eyJ1IjoiaXNoeWNhIiwiYSI6IktmMTRCWVkifQ.JiIqdtUlmU7sER58XDnQIg'
}).addTo(map);


    
    /* -- tutorial tryout --*/
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);

// Initialise the draw control and pass it the FeatureGroup of editable layers
var drawControl = new L.Control.Draw({
    edit: {
        featureGroup: drawnItems
    }
});
map.addControl(drawControl);

map.on('draw:created', function (e) {
    /*var type = e.layerType,
        layer = e.layer;

    if (type === 'marker') {
        // Do marker specific actions
    }

    // Do whatever else you need to. (save to db, add to map etc)
    map.addLayer(layer);
    */

    var feature = e.layer.toGeoJSON();
    feature.properties = { color:'#ffcc00', height:100 };
    var geoJSON = { type:'FeatureCollection', features:[feature] };
    new OSMBuildings(map).set(geoJSON);
});


    /*--- three.js --*/
    var canvasWidth = window.innerWidth/2;
    var canvasHeight = window.innerHeight*0.9;

    var scene = new THREE.Scene(); 
    var camera = new THREE.PerspectiveCamera( 75, canvasWidth / canvasHeight, 0.1, 1000 ); 
    var renderer = new THREE.WebGLRenderer(); 
    renderer.setSize( canvasWidth, canvasHeight ); 
    document.body.appendChild( renderer.domElement );
    $('canvas').css({"float":"left","width":"50%"});



});//document ready

